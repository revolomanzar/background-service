import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Image,
  ToastAndroid,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

const MapVieww = () => {
  const mapRef = useRef(null);
  const [init, setInit] = useState({
    latitude: 19.2390265,
    longitude: 72.8618129,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121,
  });

  const [location, setLocation] = useState({
    latitude: 19.2390265,
    longitude: 72.8618129,
  });

  useEffect(() => {}, []);

  const position = [
    {
      latitude: 19.2390265,
      longitude: 72.8618129,
    },
    {
      latitude: 19.229029,
      longitude: 72.863338,
    },
    {
      latitude: 19.221121,
      longitude: 72.86545,
    },
    {
      latitude: 19.2125,
      longitude: 72.868346,
    },
    {
      latitude: 19.208728,
      longitude: 72.867872,
    },
    {
      latitude: 19.206712,
      longitude: 72.866128,
    },
    {
      latitude: 19.188641,
      longitude: 72.858455,
    },
    {
      latitude: 19.169875,
      longitude: 72.858894,
    },
    {
      latitude: 19.161573,
      longitude: 72.857831,
    },
    {
      latitude: 19.144891,
      longitude: 72.855851,
    },
    {
      latitude: 19.130065,
      longitude: 72.855217,
    },
  ];

  const changeLocation = () => {
    position.map((dat, i) => {
      setTimeout(() => {
        ToastAndroid.show(
          dat.latitude + ', ' + dat.longitude,
          ToastAndroid.SHORT,
        );
        setLocation(dat);
        mapRef.current?.animateCamera({
          center: {
            latitude: dat.latitude,
            longitude: dat.longitude,
          },
        });
      }, i * 3000);
    });
    setTimeout(() => {
      console.log('done');
    }, position.length * 3000);
  };

  const mapPressed = data => {
    mapRef.current?.animateCamera({
      center: {
        latitude: data.nativeEvent.coordinate.latitude,
        longitude: data.nativeEvent.coordinate.longitude,
      },
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor="transparent"
        translucent={true}
        barStyle="dark-content"
      />
      <MapView
        ref={mapRef}
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        initialRegion={init}
        // onPress={mapPressed}
      >
        <Marker
          coordinate={location}
          style={{justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={{
              width: 80,
              height: 80,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('./Assets/truck.png')}
              style={{height: 80, width: 80}}
              resizeMode="contain"
            />
          </View>
        </Marker>
      </MapView>
      <TouchableOpacity
        onPress={changeLocation}
        style={{
          width: 60,
          height: 40,
          backgroundColor: 'dodgerblue',
          position: 'absolute',
          bottom: 20,
          right: 20,
          zIndex: 1,
          borderRadius: 5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View>
          <Text style={{color: 'white', fontWeight: 'bold', fontSize: 12}}>
            Start
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default MapVieww;
