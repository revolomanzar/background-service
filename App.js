import React, {useEffect} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {PermissionsAndroid} from 'react-native';

import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import RNLocation from 'react-native-location';
import MapVieww from './MapVieww';

const App = () => {
  const requestPermission = async () => {
    const backgroundgranted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
      {
        title: 'Background Location Permission',
        message:
          'We need access to your location ' +
          'so you can get live quality updates.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (backgroundgranted === PermissionsAndroid.RESULTS.GRANTED) {
      //do your thing!
      console.log('hello');
    }
  };

  RNLocation.configure({
    distanceFilter: 1, // Meters
    desiredAccuracy: {
      ios: 'best',
      android: 'balancedPowerAccuracy',
    },
    // Android only
    androidProvider: 'auto',
    interval: 1000,
    fastestInterval: 1000,
    maxWaitTime: 1000,
    // iOS Only
    // activityType: 'other',
    // allowsBackgroundLocationUpdates: false,
    // headingFilter: 1, // Degrees
    // headingOrientation: 'portrait',
    // pausesLocationUpdatesAutomatically: false,
    // showsBackgroundLocationIndicator: false,
  });

  let locationSubscription = null;
  let locationTimeout = null;

  ReactNativeForegroundService.add_task(
    () => {
      RNLocation.requestPermission({
        ios: 'whenInUse',
        android: {
          detail: 'fine',
        },
      }).then(granted => {
        console.log('Location Permissions: ', granted);
        if (granted) {
          locationSubscription && locationSubscription();
          locationSubscription = RNLocation.subscribeToLocationUpdates(
            ([locations]) => {
              locationSubscription();
              locationTimeout && clearTimeout(locationTimeout);
              console.log(locations);
            },
          );
        } else {
          locationSubscription && locationSubscription();
          locationTimeout && clearTimeout(locationTimeout);
          console.log('no permissions to obtain location');
        }
      });
    },
    {
      delay: 1000,
      onLoop: true,
      taskId: 'taskid',
      onError: e => console.log('Error logging:', e),
    },
  );

  useEffect(() => {
    requestPermission();
  }, []);

  const startService = () => {
    console.log('started');
    ReactNativeForegroundService.start({
      id: 144,
      title: 'Foreground Service',
      message: 'you are online!',
    });
  };

  const stopService = () => {
    console.log('Stopped');
    ReactNativeForegroundService.stop({
      id: 144,
      title: 'Foreground Service',
      message: 'you are online!',
    });
  };

  return <MapVieww />;
};

export default App;
